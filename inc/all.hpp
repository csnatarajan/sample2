#ifndef __DEFINE__
#define __DEFINE__

#include <mpi.h>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <cstdlib>
#include <algorithm>
#include "mkl.h"
#include <float.h>
#include <math.h>
#include <iomanip>
#include "arrays.hpp"
#include "structs.hpp"
#include "mkl_lapack.h"
#include "mkl_vsl.h"
#include "mkl_types.h"
using namespace std;
extern void Comm_create3D_NP(MPI_Comm comm, PComm &Comm, int *ndims);
extern void get_3d_3df(t ***data, f ***data2, int start, int end, size_t *dim_siz);
extern void get_1d_f(t ***data, f *data2, int start, int end, size_t *dim_siz);
extern void get_1df_3d(const MAT1D<float> &, MAT3D<unsigned short> &, int *);
extern void write_file_short(MPI_Comm comm, char *name, int count, MPI_Offset view, int start, unsigned short ***array);
extern void write_file_float(MPI_Comm comm, char *name, int count, MPI_Offset view, int start, float ***array);
extern void exchange_short(MAT3D <unsigned short> & image, const PComm & mycomm);
extern void read_data_ghost(MAT3D <unsigned short> & image, const MPI_Comm & comm, PComm & mycomm, std::string name);
extern void read_data_ghost(MAT3D <unsigned char> & image, const MPI_Comm & comm, PComm & mycomm, std::string name);
extern void read_data_ghost(MAT3D <float> & image, const MPI_Comm & comm, PComm & mycomm, std::string name);
extern void write_data(const MAT3D <unsigned short> & image, const PComm & mycomm, std::string name);
extern void write_data(const MAT3D <double> & image, const PComm & mycomm, std::string name);
extern void write_data_ghost(MAT3D <double> & image, const PComm & mycomm, std::string name);
extern void segmentation(MAT3D<unsigned char> & image, const int & fore, const int & back, const PComm & mycomm);
class TableHelp
{
public:
	TableHelp()
	:pagewidth_(80)
	{}
	void setpagewidth(int pw) const {pagewidth_ = pw;}
	std::string line() const;
	std::string bigline() const;
	std::string starline() const;
private:
	mutable int pagewidth_;
};
#endif
