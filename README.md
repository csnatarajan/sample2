# Sample 2 #

A parallel Eikonal equation solver for ray tracing on a domain of interest. Domain is input as a label file in 3D binary format. Uses fast sweeping with unit velocity to compute distance transform. Foreground voxel and background voxel can be specified.

### Dependencies ###

* Teuchos for CLP
* Intel MKL for memory allocation.