#include "all.hpp"
#define min(a,b) (a)>(b)?(b):(a)
#define max(a,b) (a)>(b)?(a):(b)
#define sort(a,b) {if ((a)>(b)) swap((a),(b));}
#define swap(a,b) {double temp=(a);(a)=(b);(b)=temp;}
#define thresh 1e-14

void init_df(MAT3D<double> & time, MAT3D<double> & velocity, const MAT3D<unsigned char> & image, MAT3D<unsigned char> & label, int fg, int bg, const PComm & mycomm)
{

        for (int i = 0; i < time.sizez; i++)
                for (int j = 0; j < time.sizey; j++)
                        for (int k = 0; k < time.sizex; k++)
				if (image.data[i][j][k] == fg)
				{
					label.data[i][j][k] = 100;
					if ((k!=0) && (image.data[i][j][k-1] != fg))
					{
						time.data[i][j][k] = 1.0;
						velocity.data[i][j][k] = 0.0;
					}
					else if ((k!=image.sizex-1) && (image.data[i][j][k+1] != fg))
					{
						time.data[i][j][k] = 1.0;
						velocity.data[i][j][k] = 0.0;
					} 
					else if ((j!=0) && (image.data[i][j-1][k] != fg)) 
					{
						time.data[i][j][k] = 1.0;
                                                velocity.data[i][j][k] = 0.0;
					}
					else if ((j!=image.sizey-1) && (image.data[i][j+1][k] != fg))
                                        {
                                                time.data[i][j][k] = 1.0;
                                                velocity.data[i][j][k] = 0.0;
                                        }
					else if ((i!=0) && (image.data[i-1][j][k] != fg))
                                        {
                                                time.data[i][j][k] = 1.0;
                                                velocity.data[i][j][k] = 0.0;
                                        }
                                        else if ((i!=image.sizez-1) && (image.data[i+1][j][k] != fg))
                                        {
                                                time.data[i][j][k] = 1.0;
                                                velocity.data[i][j][k] = 0.0;
                                        }
					else
					{
						time.data[i][j][k] = DBL_MAX;
						velocity.data[i][j][k] = 1.0;
					}
				}
				else
				{
					time.data[i][j][k] = 0.0;
					velocity.data[i][j][k] = 0.0;
				}
}

void exchange(MAT3D <double> & Data, const PComm & mycomm)
{
        MPI_Status status;
	MAT3D <double> vec(Data.sizex, Data.sizey, 4);

	if (mycomm.rank > 0)
        {
                MPI_Sendrecv(&Data.data[0][0][0], Data.sizex*Data.sizey, MPI_DOUBLE, mycomm.rank-1, 123,
                        &vec.data[0][0][0], vec.sizex*vec.sizey, MPI_DOUBLE, mycomm.rank-1, 125, mycomm.comm, &status);
                MPI_Sendrecv(&Data.data[1][0][0], Data.sizex*Data.sizey, MPI_DOUBLE, mycomm.rank-1, 124,
                        &vec.data[1][0][0], vec.sizex*vec.sizey, MPI_DOUBLE, mycomm.rank-1, 126, mycomm.comm, &status);

        }

        if (mycomm.rank < mycomm.nprocs-1)
        {
                MPI_Sendrecv(&Data.data[Data.sizez-1][0][0], Data.sizex*Data.sizey, MPI_DOUBLE, mycomm.rank+1, 125,
                        &vec.data[2][0][0], vec.sizex*vec.sizey, MPI_DOUBLE, mycomm.rank+1, 123, mycomm.comm, &status);
                MPI_Sendrecv(&Data.data[Data.sizez-2][0][0], Data.sizex*Data.sizey, MPI_DOUBLE, mycomm.rank+1, 126,
                        &vec.data[3][0][0], vec.sizex*vec.sizey, MPI_DOUBLE, mycomm.rank+1, 124, mycomm.comm, &status);

        }

	if (mycomm.rank < mycomm.nprocs-1)
		for (int i = 0; i < vec.sizey; i++)
                        for (int j = 0; j < vec.sizex; j++)
			{
				if (vec.data[3][i][j] < Data.data[Data.sizez-1][i][j])
					Data.data[Data.sizez-1][i][j] = vec.data[3][i][j];
				if (vec.data[2][i][j] < Data.data[Data.sizez-2][i][j])
                                        Data.data[Data.sizez-2][i][j] = vec.data[2][i][j];
			}	

	if (mycomm.rank > 0)
                for (int i = 0; i < vec.sizey; i++)
                        for (int j = 0; j < vec.sizex; j++)
                        {       
                                if (vec.data[0][i][j] < Data.data[1][i][j])
                                        Data.data[1][i][j] = vec.data[0][i][j];
                                if (vec.data[1][i][j] < Data.data[0][i][j])
                                        Data.data[0][i][j] = vec.data[1][i][j];
                        }
}


double get_nrm(const MAT3D <double> & Data, const PComm & mycomm)
{

	MPI_Status status;
	MAT1D <double> rvec(mycomm.nprocs);
	MAT3D <double> vec(Data.sizex, Data.sizey, 4);
	
	double val = 0.;

        std::fill(rvec.data, rvec.data+rvec.sizex, 0.);

        if (mycomm.rank > 0)
	{
                MPI_Sendrecv(&Data.data[0][0][0], Data.sizex*Data.sizey, MPI_DOUBLE, mycomm.rank-1, 123, 
			&vec.data[0][0][0], vec.sizex*vec.sizey, MPI_DOUBLE, mycomm.rank-1, 125, mycomm.comm, &status);
		MPI_Sendrecv(&Data.data[1][0][0], Data.sizex*Data.sizey, MPI_DOUBLE, mycomm.rank-1, 124,
                        &vec.data[1][0][0], vec.sizex*vec.sizey, MPI_DOUBLE, mycomm.rank-1, 126, mycomm.comm, &status);

	}

        if (mycomm.rank < mycomm.nprocs-1)
	{
                MPI_Sendrecv(&Data.data[Data.sizez-1][0][0], Data.sizex*Data.sizey, MPI_DOUBLE, mycomm.rank+1, 125, 
			&vec.data[2][0][0], vec.sizex*vec.sizey, MPI_DOUBLE, mycomm.rank+1, 123, mycomm.comm, &status);
		MPI_Sendrecv(&Data.data[Data.sizez-2][0][0], Data.sizex*Data.sizey, MPI_DOUBLE, mycomm.rank+1, 126,
                        &vec.data[3][0][0], vec.sizex*vec.sizey, MPI_DOUBLE, mycomm.rank+1, 124, mycomm.comm, &status);	

	}

        if (mycomm.rank < mycomm.nprocs-1)
                for (int i = 0; i < Data.sizey; i++)
                        for(int j = 0; j < Data.sizex; j++)
				val += fabs(Data.data[Data.sizez-1][i][j] - vec.data[3][i][j])
					+fabs(Data.data[Data.sizez-2][i][j] - vec.data[2][i][j]); 

	MPI_Gather(&val, 1, MPI_DOUBLE, rvec.data, 1, MPI_DOUBLE, 0, mycomm.comm);	

        if (mycomm.rank == 0)
                return (cblas_dnrm2(rvec.sizex, rvec.data, 1));
        else
                return 0.;
}

inline void get_time2(MAT3D<double> & time, const MAT3D<double> & v, const int i, const int j, const int k)
{
double T[3], term1 =0., term2=0., term3=0., term4=0., temp=0., checkrhs, maxt, left, right, north, south, front, back, up, down;
int n=2;

        if ((i-1) >= 0) 
		left = time.data[i-1][j][k];
	else
		left  = DBL_MAX;
		
        if ((i+1) < time.sizez) 
		right = time.data[i+1][j][k];
	else
		right = DBL_MAX;

        if ((j-1) >= 0) 
		north = time.data[i][j-1][k];
	else
		north = DBL_MAX;

        if ((j+1) < time.sizey) 
		south = time.data[i][j+1][k];
	else
		south = DBL_MAX;
	
	if ((k-1) >= 0)
		up = time.data[i][j][k-1];
	else
		up = DBL_MAX;

	if ((k+1) < time.sizex)
		down = time.data[i][j][k+1];
	else
		down = DBL_MAX;


        T[0] = min(left,right);
        T[1] = min(north,south);
	T[2] = min(up, down);

        sort(T[0],T[1]); sort(T[1], T[2]); sort(T[0], T[1]);
//Calculate the diff (F^2 - sig(a_m - a_k)^2. a_m = max(a_k)
	
	term1 = T[0]+1./(v.data[i][j][k]);
	term2 = ((T[0]+T[1])+sqrt(2./(v.data[i][j][k]*v.data[i][j][k]) - (T[0]-T[1])*(T[0]-T[1])))/2.;
	term3 = ((T[0]+T[1]+T[2])+sqrt(3./(v.data[i][j][k]*v.data[i][j][k]) - (T[0]-T[1])*(T[0]-T[1]) - (T[0]-T[2])*(T[0]-T[2]) - (T[2]-T[1])*(T[2]-T[1])))/3;

	if ((term1 < T[1]) && (term1 < time.data[i][j][k]))
		time.data[i][j][k] = term1;
	else if ((term2 <T[2]) && (term2 < time.data[i][j][k]))
		time.data[i][j][k] = term2;
	else if (term3 < time.data[i][j][k])
		time.data[i][j][k] = term3;
	
}

inline void sweep1(MAT3D <double> & time, const MAT3D <double> & velocity, const MAT3D <unsigned char> & label)
{
	for (int i = 0; i < time.sizez; i++)
                for (int j = 0; j < time.sizey; j++)
			for (int k = 0; k < time.sizex; k++)
				if (label.data[i][j][k] == 100)
	                                get_time2(time, velocity, i, j, k);
}

inline void sweep2(MAT3D <double> & time, const MAT3D <double> & velocity, const MAT3D <unsigned char> & label)
{
        for (int i = 0; i < time.sizez; i++)
                for (int j = 0; j < time.sizey; j++)
                        for (int k = time.sizex-1; k >=0; k--)
				if (label.data[i][j][k] == 100)
	                                get_time2(time, velocity, i, j, k);
}

inline void sweep3(MAT3D <double> & time, const MAT3D <double>  & velocity, const MAT3D <unsigned char> & label)
{
        for (int i = 0; i < time.sizez; i++)
                for (int j = time.sizey-1; j >=0; j--)
                        for (int k = 0; k < time.sizex; k++)
				if (label.data[i][j][k] == 100)
        	                        get_time2(time, velocity, i, j, k);
}

inline void sweep4(MAT3D <double> & time, const MAT3D <double> & velocity, const MAT3D <unsigned char> & label)
{
        for (int i = 0; i < time.sizez; i++)
                for (int j = time.sizey-1; j >= 0; j--)
                        for (int k = time.sizex-1; k >= 0; k--)
				if (label.data[i][j][k] == 100)	
	                                get_time2(time, velocity, i, j, k);
}

inline void sweep5(MAT3D <double> & time, const MAT3D <double> & velocity, const MAT3D <unsigned char> & label)
{
        for (int i = time.sizez-1; i >= 0; i--)
                for (int j = 0; j < time.sizey; j++)
                        for (int k = 0; k < time.sizex; k++)
				if (label.data[i][j][k] == 100)
	                                get_time2(time, velocity, i, j, k);
}

inline void sweep6(MAT3D <double> & time, const MAT3D <double> & velocity, const MAT3D <unsigned char> & label)
{
        for (int i = time.sizez-1; i >= 0; i--)
                for (int j = 0; j < time.sizey; j++)
                        for (int k = time.sizex-1; k >=0; k--)
				if (label.data[i][j][k] == 100)
	                                get_time2(time, velocity, i, j, k);
}

inline void sweep7(MAT3D <double> & time, const MAT3D <double> & velocity, const MAT3D <unsigned char> & label)
{
        for (int i = time.sizez-1; i >= 0; i--)
                for (int j = time.sizey-1; j >=0; j--)
                        for (int k = 0; k < time.sizex; k++)
				if (label.data[i][j][k] == 100)
	                                get_time2(time, velocity, i, j, k);
}

inline void sweep8(MAT3D <double> & time, const MAT3D <double> & velocity, const MAT3D <unsigned char> & label)
{
        for (int i = time.sizez-1; i >= 0; i--)
                for (int j = time.sizey-1; j >= 0; j--)
                        for (int k = time.sizex-1; k >=0; k--)
				if (label.data[i][j][k] == 100)
	                                get_time2(time, velocity, i, j, k);
}



inline void sweep(MAT3D <double> & time, const MAT3D <double> & velocity, const MAT3D <unsigned char> & label)
{
	sweep1(time, velocity, label);
	sweep2(time, velocity, label);
	sweep3(time, velocity, label);
	sweep4(time, velocity, label);
	sweep5(time, velocity, label);
	sweep6(time, velocity, label);
	sweep7(time, velocity, label);
	sweep8(time, velocity, label);
}

void update_time(MAT3D<double> & time, const MAT3D<double> & velocity, const MAT3D<unsigned char> & label, 
		 const PComm & mycomm)
{
	double norm;
	do
        {
                static int iter = 0;
                sweep(time, velocity, label);
                norm = get_nrm(time, mycomm);
		if (mycomm.rank == 0)
                	std::cout << "\t\t\t\t" << "Norm is " << norm << " and iteration is " << iter << std::endl;
                exchange(time, mycomm);
                MPI_Bcast(&norm, 1, MPI_DOUBLE, 0, mycomm.comm);
                iter++;
        } while (norm > thresh);
} 


void segment_df(MAT3D<double> & time, MAT3D<unsigned char> & label, const MAT3D<double> & velocity, const PComm & mycomm)
{
        update_time(time, velocity, label, mycomm);
}

pair<ST, ST> minmax(const MAT3D <ST> & Data)
{
pair<ST, ST> values;
long i, N; 
	N = Data.sizex*Data.sizey*Data.sizez;
	if (N%2 == 0)
	{        
		if (Data.space[0] > Data.space[1])    
		{
			values.second = Data.space[0];
			values.first = Data.space[1];
		} 
		else
		{
			values.first = Data.space[0];
			values.second = Data.space[1];
		}
		i = 2;
	} 
	else
	{
		values.first = Data.space[0];
		values.second = Data.space[0];
		i = 1;  
	}
	while (i < N-1) 
	{         
		if (Data.space[i] > Data.space[i+1])         
		{
			if(Data.space[i] > values.second)       
				values.second = Data.space[i];
			if(Data.space[i+1] < values.first)         
				values.first = Data.space[i+1];
		}
		else        
		{
			if (Data.space[i+1] > values.second)  
				values.second = Data.space[i+1];
			if (Data.space[i] < values.first)         
				values.first = Data.space[i];
		}       
		i += 2; 
	}           
return values;
}   
 
template<class T>
void histogram(const MAT3D<T> & Data, const int & bins, const string & fname, const PComm & mycomm)
{
	pair<ST, ST> values;
	double gmax, gmin, gwidth;
	values = minmax(Data);

	MPI_Reduce(&values.second, &gmax, 1, MPI_DOUBLE, MPI_MAX, 0, mycomm.comm);
	MPI_Reduce(&values.first, &gmin, 1, MPI_DOUBLE, MPI_MIN, 0, mycomm.comm);

	gwidth = gmax-gmin;
	MPI_Bcast(&gwidth, 1, MPI_DOUBLE, 0, mycomm.comm);

	MAT1D<double> temp_hist(bins+1), hist(bins+1); 

	fill(hist.data, hist.data+hist.sizex, 0);
	fill(temp_hist.data, temp_hist.data+temp_hist.sizex, 0.);

	ST temp = (1.*bins)/(gwidth);
	int start = (mycomm.rank == 0)?0:1;
        int end = (mycomm.rank == mycomm.nprocs-1)?(Data.sizez):(Data.sizez-1);
	for(int i = start; i < end; i++)
		for(int j = 0; j < Data.sizey; j++)
			for(int k = 0; k < Data.sizex; k++)
				temp_hist.data[static_cast<int> (floor(Data.data[i][j][k]*temp))] += 1.0;


	MPI_Reduce(temp_hist.data, hist.data, bins+1, MPI_DOUBLE, MPI_SUM, 0, mycomm.comm);
	if (mycomm.rank == 0)
	{
		fstream f; f.open(fname.c_str(), ios::out);

		for (int i = 0; i <= bins; i++)
			f << fixed << gmin+i/temp << "\t" << hist.data[i] << endl;
		f.close();
	}
}


void segmentation(MAT3D<unsigned char> & image, const int & fore, const int & back, const PComm & mycomm)
{
	
	double norm;
	MAT3D<unsigned char> label(image.sizex, image.sizey, image.sizez);
        std::fill(&label.data[0][0][0], &label.data[0][0][0]+(label.sizex*label.sizey*label.sizez), 0.);	
	MAT3D <double> time(image.sizex, image.sizey, image.sizez), velocity(image.sizex, image.sizey, image.sizez);
	init_df(time, velocity, image, label, fore, back, mycomm);
	segment_df(time, label, velocity, mycomm);
	histogram(time, 2000, "histogram.dat", mycomm);
	write_data_ghost(time, mycomm, "dmap.raw");
}
