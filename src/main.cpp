#include "all.hpp"
#define eps 1e-10
typedef double                            ST;
typedef int 				 ordinal;
typedef Teuchos::ScalarTraits<ST>        SCT;
typedef SCT::magnitudeType                MT;
typedef Epetra_MultiVector                MV;
typedef Epetra_Operator                   OP;
typedef Belos::MultiVecTraits<ST,MV>     MVT;
typedef Belos::OperatorTraits<ST,MV,OP>  OPT;
using namespace std;
using Teuchos::ParameterList;
using Teuchos::RCP;
using Teuchos::rcp;

int main(int argc, char *argv[])
{


        PComm mycomm;
  	int nprocx, nprocy, nprocz, nprocs, rank;


  	MPI_Init(&argc,&argv);

	MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

        Teuchos::oblackholestream nullstream;
        std::ostream &out = (rank == 0)?std::cout:nullstream;

	Teuchos::CommandLineProcessor clp;
	clp.setDocString("This program solves the eikonal equation (ray tracing) with unit velocity\n A simulation can be run by issuing mpirun -np <num procs> -perhost <number of cores per proc for RR or batch sequential> RT --nx=<#points in x> --ny=<points in y> --nz=<#points in z> --fname=<name of file contaiing > --fore <foreground voxel> --back <background voxel>");

	int nx = 390;
	clp.setOption("nx", &nx, "Image size in X-direction");

	int ny = 390;
	clp.setOption("ny", &ny, "Image size in Y-direction");

	int nz = 515;
	clp.setOption("nz", &nz, "Image size in Z-Direction");

	int fore = 0;
	clp.setOption("fore", &fore, "Foreground label---AKA label on which you want to compute the map");

	int back = 1;
	clp.setOption("back", &back, "Background label--AKA label to against which the map should be computed");

	std::string fname("fb.in");
        clp.setOption("fname", &fname, "Name of input file");


	clp.recogniseAllOptions(true);
	clp.throwExceptions(false);

	Teuchos::CommandLineProcessor::EParseCommandLineReturn parseReturn = clp.parse(argc, argv);
	if(parseReturn == Teuchos::CommandLineProcessor::PARSE_HELP_PRINTED) return 0;
	if(parseReturn != Teuchos::CommandLineProcessor::PARSE_SUCCESSFUL) return 1;

		
	long count = (nz/nprocs);
	long np;
	np = (nz - (nprocs*count));
	count += (rank < np)?1:0;
	mycomm.np = np;	
	long size = ((rank == 0) || (rank == nprocs-1))?(count+1):(count+2);

	MAT3D <unsigned char> image(nx, ny, size);

        mycomm.exist = false;
	RCP<TableHelp> draw = rcp(new TableHelp);
	out << draw->bigline() << std::endl;

	RCP<Time> iotime = TimeMonitor::getNewTimer("I/O time");
        {
                TimeMonitor Timer(*iotime);
                read_data_ghost(image, MPI_COMM_WORLD, mycomm, const_cast<char *> (fname.c_str()));
        }
	out << "Finished reading data ..." << std::endl;	
	
	if (mycomm.exist)
	{
		out << "File name is " << fname << std::endl;
		out << "Size in X  is " << nx << endl;
		out << "Size in Y  is " << ny << endl;
		out << "Size in Z  is " << nz << endl;

		MPI_Comm_size(mycomm.comm, &nprocs); Epetra_MpiComm Comm(mycomm.comm);

		out << "Using " << Comm.NumProc() <<" processors" << std::endl;		
		RCP<Time> edtime = TimeMonitor::getNewTimer("EDT time");
        	{
			TimeMonitor Timer(*edtime);
			segmentation(image, fore, back, mycomm);
		}
	}

	TimeMonitor::summarize(out,true);
	MPI_Finalize();
  
}
