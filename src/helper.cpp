#include "all.hpp"
std::string TableHelp::line() const
{
        std::ostringstream oss;
        for(int i = 0; i < pagewidth_; i++)
        {
                oss << "-";
        }
        return oss.str();
}

std::string TableHelp::bigline() const
{
        std::ostringstream oss;
        for(int i = 0; i < pagewidth_; i++)
        {
                oss << "=";
        }
        return oss.str();
}

std::string TableHelp::starline() const
{
        std::ostringstream oss;
        for(int i = 0; i < pagewidth_; i++)
        {
                oss << "*";
        }
        return oss.str();
}
void read_data_ghost(MAT3D <float> & image, const MPI_Comm & comm, PComm & mycomm, std::string name)
{
	MPI_Datatype filetype;
	MPI_File f;
	MPI_Comm subcomm1, subcomm2;
	MPI_Status status;
	int rank, nprocs, key;
	int readcount;
	long zdim = image.sizez-2;

	MPI_Comm_rank(comm, &rank); MPI_Comm_size(comm, &nprocs);

        MPI_File_open(comm, const_cast<char *> (name.c_str()), MPI_MODE_RDONLY, MPI_INFO_NULL, &f);

        MPI_Offset view = image.sizex*image.sizey*zdim*rank*sizeof(float);

	long count = image.sizex*image.sizey*zdim;
	MPI_Type_contiguous(count, MPI_FLOAT, &filetype); MPI_Type_commit(&filetype);

        MPI_File_set_view(f, view, MPI_FLOAT, filetype, "native", MPI_INFO_NULL);
	
	int start = (rank > 0)?1:0;
	MPI_File_read_all(f, &image.data[start][0][0], count, MPI_FLOAT, &status);	

        MPI_Get_count(&status, MPI_FLOAT, &readcount); MPI_File_close(&f);

	mycomm.readcount = readcount;

        key = readcount>0?1:2; MPI_Comm_split(comm, key, rank, readcount>0?&subcomm1:&subcomm2);

	if (readcount > 0)
	{
		MPI_Status status;
		MPI_Comm_size(subcomm1, &nprocs);
//		if (rank == 0)
//			std::cout << "\t\t\t\t Using " << nprocs << " processors for calculation " << std::endl;

		int ndims[3] = {nprocs, 1, 1};

		Comm_create3D_NP(subcomm1, mycomm, ndims);

		
		if (mycomm.rank > 0)
			MPI_Sendrecv(&image.data[1][0][0], image.sizex*image.sizey, MPI_FLOAT, mycomm.rank-1, 123, 
				     &image.data[0][0][0], image.sizex*image.sizey, MPI_FLOAT, mycomm.rank-1, 
				     124, mycomm.comm,&status);
		if (mycomm.rank < mycomm.nprocs-1)
                	MPI_Sendrecv(&image.data[image.sizez-2][0][0], image.sizex*image.sizey, MPI_FLOAT, mycomm.rank+1, 124					 ,&image.data[image.sizez-1][0][0], image.sizex*image.sizey, MPI_FLOAT, mycomm.rank+1, 123				      , mycomm.comm, &status);
		if ((mycomm.rank == 0) || (mycomm.rank == mycomm.nprocs-1))
			image.sizez = readcount/(image.sizex*image.sizey) + 1;
                mycomm.exist = true;
        }
}
void read_data_ghost2(MAT3D <unsigned char> & image, const MPI_Comm & comm, PComm & mycomm, std::string name)
{
	MPI_Datatype filetype;
	MPI_File f;
	MPI_Comm subcomm1, subcomm2;
	MPI_Status status;
	int rank, nprocs, key;
	int readcount;

	MPI_Comm_rank(comm, &rank); MPI_Comm_size(comm, &nprocs);

	long zdim = (rank == 0)?(image.sizez-1):(image.sizez-2);

        MPI_File_open(comm, const_cast<char *> (name.c_str()), MPI_MODE_RDONLY, MPI_INFO_NULL, &f);

        MPI_Offset view = image.sizex*image.sizey*zdim*rank*sizeof(char);

	long count = image.sizex*image.sizey*zdim;
	MPI_Type_contiguous(count, MPI_CHAR, &filetype); MPI_Type_commit(&filetype);

        MPI_File_set_view(f, view, MPI_CHAR, filetype, "native", MPI_INFO_NULL);
	
	int start = (rank > 0)?1:0;
	MPI_File_read_all(f, &image.data[start][0][0], count, MPI_CHAR, &status);	

        MPI_Get_count(&status, MPI_CHAR, &readcount); MPI_File_close(&f);

	mycomm.readcount = readcount;

        key = readcount>0?1:2; MPI_Comm_split(comm, key, rank, readcount>0?&subcomm1:&subcomm2);

	if (readcount > 0)
	{
		MPI_Status status;
		MPI_Comm_size(subcomm1, &nprocs);

		int ndims[3] = {nprocs, 1, 1};

		Comm_create3D_NP(subcomm1, mycomm, ndims);

		
		if (mycomm.rank > 0)
			MPI_Sendrecv(&image.data[1][0][0], image.sizex*image.sizey, MPI_CHAR, mycomm.rank-1, 123, 
				     &image.data[0][0][0], image.sizex*image.sizey, MPI_CHAR, mycomm.rank-1, 
				     124, mycomm.comm,&status);
		if (mycomm.rank < mycomm.nprocs-1)
                	MPI_Sendrecv(&image.data[image.sizez-2][0][0], image.sizex*image.sizey, MPI_CHAR, mycomm.rank+1, 124					 ,&image.data[image.sizez-1][0][0], image.sizex*image.sizey, MPI_CHAR, mycomm.rank+1, 123				      , mycomm.comm, &status);
		if ((mycomm.rank == 0) || (mycomm.rank == mycomm.nprocs-1))
			image.sizez = readcount/(image.sizex*image.sizey) + 1;
                mycomm.exist = true;
        }
}

void read_data_ghost(MAT3D <unsigned char> & image, const MPI_Comm & comm, PComm & mycomm, std::string name)
{
	MPI_Datatype filetype;
	MPI_File f;
	MPI_Comm subcomm1, subcomm2;
	MPI_Status status;
	int rank, nprocs, key;
	int readcount;

	MPI_Comm_rank(comm, &rank); MPI_Comm_size(comm, &nprocs);

	long zdim = ((rank == 0) || (rank == nprocs-1))?(image.sizez-1):(image.sizez-2);
	MPI_Offset view;

	if (rank < mycomm.np)
		view = image.sizex*image.sizey*zdim*rank*sizeof(char);
	else
		view = image.sizex*image.sizey*zdim*(rank-mycomm.np)*sizeof(char) + image.sizex*image.sizey*(zdim+1)*mycomm.np*sizeof(char);
        MPI_File_open(comm, const_cast<char *> (name.c_str()), MPI_MODE_RDONLY, MPI_INFO_NULL, &f);

	long count = image.sizex*image.sizey*zdim;

	MPI_Type_contiguous(count, MPI_CHAR, &filetype); MPI_Type_commit(&filetype);

        MPI_File_set_view(f, view, MPI_CHAR, filetype, "native", MPI_INFO_NULL);
	
	int start = (rank > 0)?1:0;
	MPI_File_read_all(f, &image.data[start][0][0], count, MPI_CHAR, &status);	
//	std::cout << "finished reading file : " << " at rank " << rank << std::endl;
        MPI_Get_count(&status, MPI_CHAR, &readcount); MPI_File_close(&f);

	mycomm.readcount = readcount;

        key = readcount>0?1:2; MPI_Comm_split(comm, key, rank, readcount>0?&subcomm1:&subcomm2);


	if (readcount > 0)
	{
		MPI_Status status;
		MPI_Comm_size(subcomm1, &nprocs);

		int ndims[3] = {nprocs, 1, 1};

		Comm_create3D_NP(subcomm1, mycomm, ndims);

		
		if (mycomm.rank > 0)
			MPI_Sendrecv(&image.data[1][0][0], image.sizex*image.sizey, MPI_CHAR, mycomm.rank-1, 123, 
				     &image.data[0][0][0], image.sizex*image.sizey, MPI_CHAR, mycomm.rank-1, 
				     124, mycomm.comm,&status);
		if (mycomm.rank < mycomm.nprocs-1)
                	MPI_Sendrecv(&image.data[image.sizez-2][0][0], image.sizex*image.sizey, MPI_CHAR, mycomm.rank+1, 124,
				     &image.data[image.sizez-1][0][0], image.sizex*image.sizey, MPI_CHAR, mycomm.rank+1, 123, 
				     mycomm.comm, &status);
		if ((mycomm.rank == 0) || (mycomm.rank == mycomm.nprocs-1))
			image.sizez = readcount/(image.sizex*image.sizey) + 1;
                mycomm.exist = true;
/*
		for (int i = 0; i < nprocs; i++)
		{
			if (mycomm.rank == i)
				std::cout << "rank is " << rank << " view is " << view/(image.sizex*image.sizey) << std::endl;
			MPI_Barrier(mycomm.comm);
		}
*/
        }
}

void read_data_ghost(MAT3D <unsigned short> & image, const MPI_Comm & comm, PComm & mycomm, std::string name)
{
	MPI_Datatype filetype;
	MPI_File f;
	MPI_Comm subcomm1, subcomm2;
	MPI_Status status;
	int rank, nprocs, key;
	int readcount;

	MPI_Comm_rank(comm, &rank); MPI_Comm_size(comm, &nprocs);
	long zdim = (rank == 0)?(image.sizez-1):(image.sizez-2);

	mycomm.rank = rank;

        MPI_File_open(comm, const_cast<char *> (name.c_str()), MPI_MODE_RDONLY, MPI_INFO_NULL, &f);

        MPI_Offset view = image.sizex*image.sizey*zdim*rank*sizeof(short);

	long count = image.sizex*image.sizey*zdim;
	MPI_Type_contiguous(count, MPI_SHORT, &filetype); MPI_Type_commit(&filetype);

        MPI_File_set_view(f, view, MPI_SHORT, filetype, "native", MPI_INFO_NULL);
	
	int start = (rank > 0)?1:0;
	MPI_File_read_all(f, &image.data[start][0][0], count, MPI_SHORT, &status);	

        MPI_Get_count(&status, MPI_SHORT, &readcount); MPI_File_close(&f);

	mycomm.readcount = readcount;

        key = readcount>0?1:2; MPI_Comm_split(comm, key, rank, readcount>0?&subcomm1:&subcomm2);

	if (readcount > 0)
	{
		MPI_Status status;
		MPI_Comm_size(subcomm1, &nprocs);

		int ndims[3] = {nprocs, 1, 1};

		Comm_create3D_NP(subcomm1, mycomm, ndims);

		
		if (mycomm.rank > 0)
			MPI_Sendrecv(&image.data[1][0][0], image.sizex*image.sizey, MPI_SHORT, mycomm.rank-1, 123
			, &image.data[0][0][0], image.sizex*image.sizey, MPI_SHORT, mycomm.rank-1, 
			124, mycomm.comm,&status);

		if (mycomm.rank < mycomm.nprocs-1)
                	MPI_Sendrecv(&image.data[image.sizez-2][0][0], image.sizex*image.sizey, MPI_SHORT, 
			mycomm.rank+1, 124, &image.data[image.sizez-1][0][0], image.sizex*image.sizey, MPI_SHORT
			, mycomm.rank+1, 123, mycomm.comm, &status);

		if (mycomm.rank == mycomm.nprocs-1)
                        image.sizez = readcount/(image.sizex*image.sizey) + 1;

                mycomm.exist = true; mycomm.endsize = mycomm.readcount;

		if (mycomm.rank == mycomm.nprocs-1)
	        	MPI_Recv(&mycomm.endsize, 1, MPI_LONG, mycomm.nprocs-2, 131, mycomm.comm, &status);
        	else if (mycomm.rank == mycomm.nprocs-2)
                	MPI_Send((void *)&(mycomm.readcount), 1, MPI_LONG, mycomm.nprocs-1, 131, mycomm.comm);

		MPI_Allreduce(&mycomm.readcount, &mycomm.total, 1, MPI_LONG, MPI_SUM, mycomm.comm);	
        }
}

void exchange_short(MAT3D <unsigned short> & image, const PComm & mycomm)
{
	MPI_Status status;
	if (mycomm.rank > 0)
		MPI_Sendrecv(&image.data[1][0][0], image.sizex*image.sizey, MPI_SHORT, mycomm.rank-1, 123,
			     &image.data[0][0][0], image.sizex*image.sizey, MPI_SHORT, mycomm.rank-1,
			     124, mycomm.comm,&status);
	if (mycomm.rank < mycomm.nprocs-1)
		MPI_Sendrecv(&image.data[image.sizez-2][0][0], image.sizex*image.sizey, MPI_SHORT, mycomm.rank+1, 124,
			&image.data[image.sizez-1][0][0], image.sizex*image.sizey, MPI_SHORT, mycomm.rank+1, 123, 
			mycomm.comm, &status);
}
void write_data_ghost(MAT3D <double> & image, const PComm & mycomm, std::string name)
{
	MPI_Datatype filetype;
	MPI_File f;
	MPI_Status status;

	long zdim = ((mycomm.rank == 0) || (mycomm.rank == mycomm.nprocs-1))?(image.sizez-1):(image.sizez-2);
	MPI_Offset view;

	if (mycomm.rank < mycomm.np)
		view = image.sizex*image.sizey*zdim*mycomm.rank*sizeof(double);
	else
		view = image.sizex*image.sizey*zdim*(mycomm.rank-mycomm.np)*sizeof(double) + image.sizex*image.sizey*(zdim+1)*mycomm.np*sizeof(double);

	MPI_File_open(mycomm.comm, const_cast<char *> (name.c_str()), MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &f);

	long count = image.sizex*image.sizey*zdim;

	MPI_Type_contiguous(mycomm.readcount, MPI_DOUBLE, &filetype); MPI_Type_commit(&filetype);

        MPI_File_set_view(f, view, MPI_DOUBLE, filetype, "native", MPI_INFO_NULL);
	
	int start = (mycomm.rank > 0)?1:0;
	MPI_File_write_all(f, &image.data[start][0][0], mycomm.readcount, MPI_DOUBLE, &status);	


}

void write_data(const MAT3D <unsigned short> & image, const PComm & mycomm, std::string name)
{
        MPI_Datatype filetype;
        MPI_File f;
        MPI_Status status;
        int start = (mycomm.rank ==0)?0:1;
        MPI_Offset view;
	long endsize; 
	MPI_Type_contiguous(mycomm.readcount, MPI_SHORT, &filetype);
        MPI_Type_commit(&filetype);
        MPI_File_open(mycomm.comm, const_cast<char *> (name.c_str()), MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &f);
	view = mycomm.endsize*mycomm.rank*sizeof(short);
        MPI_File_set_view(f, view, MPI_SHORT, filetype, "native", MPI_INFO_NULL);
        MPI_File_write_all(f, &image.data[start][0][0], mycomm.readcount, MPI_SHORT, &status);
        MPI_File_close(&f);

}

void write_data(const MAT3D <unsigned char> & image, const PComm & mycomm, std::string name)
{
        MPI_Datatype filetype;
        MPI_File f;
        MPI_Status status;
	MPI_Offset view;
       	long endsize; 
	int start = (mycomm.rank ==0)?0:1;
        MPI_Type_contiguous(mycomm.readcount, MPI_CHAR, &filetype);
        MPI_Type_commit(&filetype);
        MPI_File_open(mycomm.comm, const_cast<char *> (name.c_str()), MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &f);
	view = mycomm.endsize*mycomm.rank*sizeof(char);
        MPI_File_set_view(f, view, MPI_CHAR, filetype, "native", MPI_INFO_NULL);
        MPI_File_write_all(f, &image.data[start][0][0], mycomm.readcount, MPI_CHAR, &status);
        MPI_File_close(&f);
}


void write_data(const MAT3D <float> & image, const PComm & mycomm, std::string name)
{
        MPI_Datatype filetype;
        MPI_File f;
        MPI_Status status;
	MPI_Offset view;
	long endsize;
        int start = (mycomm.rank ==0)?0:1;
        MPI_Type_contiguous(mycomm.readcount, MPI_FLOAT, &filetype);
        MPI_Type_commit(&filetype);
        MPI_File_open(mycomm.comm, const_cast<char *> (name.c_str()), MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &f);
	view = mycomm.endsize*mycomm.rank*sizeof(float);
        MPI_File_set_view(f, view, MPI_FLOAT, filetype, "native", MPI_INFO_NULL);
        MPI_File_write_all(f, &image.data[start][0][0], mycomm.readcount, MPI_FLOAT, &status);
        MPI_File_close(&f);

}

void write_data(const MAT3D <double> & image, const PComm & mycomm, std::string name)
{
        MPI_Datatype filetype;
        MPI_File f;
        MPI_Status status;
        MPI_Offset view;
        long endsize;
        int start = (mycomm.rank ==0)?0:1;
        MPI_Type_contiguous(mycomm.readcount, MPI_DOUBLE, &filetype);
        MPI_Type_commit(&filetype);
        MPI_File_open(mycomm.comm, const_cast<char *> (name.c_str()), MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &f);
	view = mycomm.endsize*mycomm.rank*sizeof(double);
        MPI_File_set_view(f, view, MPI_DOUBLE, filetype, "native", MPI_INFO_NULL);
        MPI_File_write_all(f, &image.data[start][0][0], mycomm.readcount, MPI_DOUBLE, &status);
        MPI_File_close(&f);

}
